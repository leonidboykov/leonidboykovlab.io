# GitLab Pages redirects

This repo contains GitLab-based files with redirects to the main Netlify static-site [deployment](https://leonidboykov.com).

## Adding redirects

To redirect a GitLab Pages project `project_name` to a `https://new_url/`, just add an entry to the `redirects.json` file

```json
{"project_name":"https://new_url/"}
```
